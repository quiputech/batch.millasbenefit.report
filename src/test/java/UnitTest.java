/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.quiputech.batch.millasbenefit.report.MillasBenefitReport;
import com.quiputech.batch.millasbenefit.report.SFTPClient;
import org.testng.annotations.Test;
/**
 *
 * @author mgaldamez
 */
public class UnitTest {
    @Test(enabled = false)    
    public void testsendSns() {
        
        MillasBenefitReport gateway = new MillasBenefitReport();
        gateway.executeReportLQTXPS();
        
    }
    
    @Test(enabled = false)    
    public void checkFileSize() {
        
        MillasBenefitReport gateway = new MillasBenefitReport();
        gateway.getFileSize("C:/opt/output/LQTXPS_0037_20201013.DAT");
        
    }
    
    @Test(enabled = false)    
    public void checkConnection() {
        try {
            boolean test = false;
            String privatekey = test ? "/opt/output/USR_NIUBIZ_GFC.ppk" : ""; 
            String username = test ? "USR_NIUBIZ_GFC" : "usr_canales";
            String password = test ? "" : "TrnsBncCnt@43!";
            String host = test ? "sftpa.intercorp.com.pe" : "192.168.30.198";
            int port = test ? 22220 : 21;
            SFTPClient sft = new SFTPClient();
            sft.connect(privatekey, username, password, host, port);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        
    }    
}
