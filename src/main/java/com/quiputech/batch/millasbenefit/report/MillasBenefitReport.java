package com.quiputech.batch.millasbenefit.report;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.util.*;
import java.text.SimpleDateFormat;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import com.quiputech.http.util.HttpResponse;
import com.quiputech.http.util.HttpUtil;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.mail.MessagingException;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MillasBenefitReport {

    private static final Logger LOGGER = LoggerFactory.getLogger(MillasBenefitReport.class);
    private static final boolean DEBUG = Boolean.parseBoolean(System.getProperty("debug", "false"));
    private static final String MILLAS_BENEFIT_COMPANY_ID = "172";
    private static final String URL_REPORT = DEBUG
        ? "https://devapi.vnforapps.com/api.utils/v1/millasBenefit/summary"
        : "http://localhost:8080/api.utils/v1/millasBenefit/summary";
    private static final String URL_SECURITY = DEBUG
        ? "https://apitestenv.vnforapps.com/api.security/v1/security"
        : "https://apiprod.vnforapps.com/api.security/v1/security";
    private static final String URL_REPORT_CONSOLIDADO = DEBUG
        ? "https://devapi.vnforapps.com/api.utils/v1/report/summary/{companyId}"
        : "http://localhost:8080/api.utils/v1/report/summary/{companyId}";
    private static final String ARN_SNS_MILLASBENEFIT = DEBUG ? 
        "arn:aws:sns:us-east-1:820370175812:email-reporte-millasbenefit" :
        "arn:aws:sns:us-east-1:589045502237:visanet_reports";
    public static final String CREDENTIALS = "Basic Z2lhbmNhZ2FsbGFyZG9AZ21haWwuY29tOkF2MyR0cnV6";
    private static final String PRIVATE_KEY = DEBUG 
        ? "/opt/output/USR_NIUBIZ_GFC.ppk"
        : "/opt/output/2801733_private_key.ppk";
    private static final String USERNAME_1 = "USR_NIUBIZ_GFC";
    private static final String HOST_1 = DEBUG 
        ? "sftpa.intercorp.com.pe"
        : "sftpe.intercorp.com.pe";
    private static final String PATH_1 = DEBUG
        ? "/IN"
        : "/home/NIUBIZ/GFC/IN/";
    private static final int PORT_1 = 22220;
    private static final String USERNAME_2 = "usr_canales";
    private static final String PASSWORD_2 = "TrnsBncCnt@43!";
    private static final String HOST_2 = "192.168.30.198";
    private static final String PATH_2 = "/data/ftp/appdwh_tdp/lqtxps";
    private static final int DEFAULT_TIMEOUT = 360000;
   
    public void upload(String fileName, String reportType) {
        try {
            boolean isLQTXPS = reportType.equals("LQTXPS");
            LOGGER.info("Uploading file to sftp");
            LOGGER.info(String.format("USERNAME: %s", isLQTXPS ? USERNAME_2 : USERNAME_1));
            LOGGER.info(String.format("HOST: %s", isLQTXPS ? HOST_2 : HOST_1));
            LOGGER.info(String.format("PORT: %d", isLQTXPS ? "0" : PORT_1));
            LOGGER.info(String.format("PORT: %d", isLQTXPS ? "0" : PORT_1));
            //sftp
            SFTPClient sft = new SFTPClient();
            if(isLQTXPS) {
                sft.connect("", USERNAME_2, PASSWORD_2, HOST_2, 0);
            } else {
                sft.connect(PRIVATE_KEY, USERNAME_1, "", HOST_1, PORT_1);
            }
            long fileSize = getFileSize(fileName);
            String path = isLQTXPS ? PATH_2 : PATH_1;
            if(fileSize > 1) {
                boolean isUpload = sft.upload(fileName, path);
                if(isUpload) {
                    LOGGER.info("Archivo subido a SFTP con exito");
                } else {
                    LOGGER.info("No se pudo subir el archivo");
                }
            }
        } catch(JSchException | SftpException e) {
            LOGGER.error(e.getLocalizedMessage());
        }
    }
    
    public void retry() {
        File temp;
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            SimpleDateFormat fileDate = new SimpleDateFormat("yyyyMMdd");
            String fileError = String.format("/opt/output/0172_LQTXPS_%s.error", fileDate.format(calendar.getTime()));
            String fileName = String.format("/opt/output/LQTXPS_0172_%s.DAT", fileDate.format(calendar.getTime()));
            temp = new File(fileError);  
            boolean exists = temp.exists();
            if (!exists){
                LOGGER.info("Nothing to do");
            } else {
                LOGGER.info("Millas Benefit Report Executing Retry...");
                //sftp
                SFTPClient sft = new SFTPClient();
                sft.connect("", USERNAME_2, PASSWORD_2, HOST_2, 0);

                boolean isUpload = sft.upload(fileName, PATH_2);
                if(isUpload) {
                    LOGGER.info("Millas Benefit Report LQTXPS Uploaded Successfully");
                    String body = String.format("Los siguientes archivos:\n0172-MILLAS BENEFIT IBK\n%s\nFueron subidos al host con éxito.", fileName);
                    sendNotification(body, "REINTENTO: Envio de Archivo Liquidacion POS Servicios - FINALIZADO OK", ARN_SNS_MILLASBENEFIT);
                    temp.delete();
                } else {
                    String body = String.format("Hubo error en la subida del archivo al host: \n"
                        + "Archivo LQTXPS_0172_%s.DAT ", fileDate.format(calendar.getTime()));
                    sendNotification(body, "REINTENTO ERROR: Envio de Archivo Liquidacion POS Servicios - ERROR", ARN_SNS_MILLASBENEFIT);
                }   
            }
        } catch (JSchException | SftpException e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    public long getFileSize(String fileName) {
        Path path = Paths.get(fileName);
        try {
            // size of a file (in bytes)
            long bytes = Files.size(path);
            LOGGER.info(String.format("Millas Benefit Report FileSize %,d bytes", bytes));
            //System.out.println(String.format("%,d bytes", bytes));
            //System.out.println(String.format("%,d kilobytes", bytes / 1024));
            return bytes;
        } catch (IOException e) {
            LOGGER.error("ERROR: Millas Benefit Report GetFileSize Failed");
            return 0;
        }
    }
    
    public void email(String subject, String message) {

        try {
            Session session = Session.getDefaultInstance(new Properties());

            MimeMessage mMessage = new MimeMessage(session);
            mMessage.setSubject(subject, "UTF-8");
            mMessage.setFrom(new InternetAddress("notificaciones@qaniubiz.com"));
            //Add to recipients
            String[] recipients = new String[] { "mgaldamez@quiputech.com", "ealcalde@niubiz.com.pe", "cmaldonado@niubiz.com.pe"}; //null;
            //recipients = parameters.get("RECIPIENTS_ERROR").split(",");
            Arrays.asList(recipients).forEach(LOGGER::info);
            InternetAddress[] aRecipients = new InternetAddress[recipients.length];

            for (int i = 0; i < aRecipients.length; i++) {
                if (recipients[i].trim().length() > 0)
                    aRecipients[i] = new InternetAddress(recipients[i].trim());
            }

            mMessage.setRecipients(Message.RecipientType.TO, aRecipients);

            MimeMultipart msg_body = new MimeMultipart("alternative");
            MimeBodyPart wrap = new MimeBodyPart();

            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(message, "text/plain; charset=UTF-8");
            msg_body.addBodyPart(htmlPart);

            wrap.setContent(msg_body);

            MimeMultipart msg = new MimeMultipart("mixed");
            mMessage.setContent(msg);

            msg.addBodyPart(wrap);


            AmazonSimpleEmailService client =
                    AmazonSimpleEmailServiceClientBuilder.standard()
                            .withRegion("us-east-1").build();

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            mMessage.writeTo(outputStream);
            RawMessage rawMessage =
                    new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

            SendRawEmailRequest rawEmailRequest =
                    new SendRawEmailRequest(rawMessage);

            LOGGER.info("SENDING EMAIL");
            client.sendRawEmail(rawEmailRequest);

        } catch (IOException | MessagingException e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private AWSCredentialsProviderChain getAWSCredentials() {

        return new AWSCredentialsProviderChain(
                new InstanceProfileCredentialsProvider(false),
                new SystemPropertiesCredentialsProvider(),
                new ProfileCredentialsProvider("development")
        );
    }
    
    public void uploadToS3(String fileName, String fileObjKeyName) throws Exception {
        try {
            BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIA36AO7O5CB3HJT4MB", "dgv89d45bga++2+TtF85BxsvW5c872DS7F7b5xDg");

            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion("us-east-1")
                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                    .build();

            PutObjectRequest request = new PutObjectRequest("pruebas-quiputech", fileObjKeyName, new File(fileName));
            s3Client.putObject(request);
        LOGGER.info("Archivo Subido con exito");
        } catch (SdkClientException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }
        
    public void sendNotification(String message, String subject, String arn_sns) {
        try {
            AmazonSNS snsClient = AmazonSNSClientBuilder.standard()
                    .withCredentials(getAWSCredentials())
                    .withRegion(Regions.US_EAST_1)
                    .build();
            snsClient.publish(arn_sns, message, subject);
            LOGGER.info("Enviando mensaje");
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }    
    
    public String getSecurityToken() {
        HttpResponse httpResponse = null;
        try {
            httpResponse = HttpUtil.sendRequest(HttpMethod.GET, "", URL_SECURITY, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, CREDENTIALS);        
        } catch(IOException ex) {
            LOGGER.info(ex.getLocalizedMessage());
        }        
        return httpResponse != null && httpResponse.getHttpCode() == 201 ? httpResponse.getResponse() : null;
    }
    
    public void executeConsolidatedReport() {
        LOGGER.info("Millas Benefit Report Consolidado");
        try {
            String accessToken = getSecurityToken();
            LOGGER.info(String.format("URL: %s", URL_REPORT));
            HttpResponse httpResponse = HttpUtil.sendRequest(HttpMethod.GET, "", URL_REPORT, MediaType.APPLICATION_JSON, DEFAULT_TIMEOUT, DEFAULT_TIMEOUT, DEFAULT_TIMEOUT,
                    HttpHeaders.AUTHORIZATION, accessToken);
            LOGGER.info(String.format("HTTP CODE: %d", httpResponse.getHttpCode()));
            LOGGER.info(String.format("RESPONSE: %s", httpResponse.getResponse()));
            if (httpResponse.getHttpCode() == 200) {
                LOGGER.info("Millas Benefit Report Consolidado Generated Succssfully");
                //sftp
                SFTPClient sft2 = new SFTPClient();
                sft2.connect(PRIVATE_KEY, USERNAME_1, "", HOST_1, PORT_1);

                String fileName = httpResponse.getResponse();
                boolean isUpload = sft2.upload(fileName, PATH_1);

                if (isUpload) {
                    LOGGER.info("Archivo subido a host Millas Benefit SFTP con exito");
                    String body = String.format("Archivo %s subido a host Millas Benefit SFTP con exito.", fileName);
                    sendNotification(body, "Reporte MILLAS BENEFIT- CONSOLIDADO ENVIADO OK", ARN_SNS_MILLASBENEFIT);
                } else {
                    String body = String.format("Hubo error en la subida del archivo al host del banco: \n"
                        + "Archivo %s ", fileName);
                    sendNotification(body, "Reporte MILLAS BENEFIT- CONSOLIDADO ENVIADO ERROR", ARN_SNS_MILLASBENEFIT);
                }
            } else {
                LOGGER.error("ERROR: NO SE PUDO GENERAR EL REPORTE CONSOLIDADO");
                sendNotification("Ocurrio un error al ejecutar el reporte consolidado Millas Benefit", "Reporte MILLAS BENEFIT - ERROR", ARN_SNS_MILLASBENEFIT);
            }
        } catch(JSchException | SftpException | IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            sendNotification("Ocurrio un error " + ex.getLocalizedMessage(), "Reporte MILLAS BENEFIT - ERROR", ARN_SNS_MILLASBENEFIT);
        }
    }
    
    public void executeReportLQTXPS() {
        LOGGER.info("Millas Benefit Report LQTXPS");
        String accessToken = getSecurityToken();
        String url = URL_REPORT_CONSOLIDADO.replace("{companyId}", MILLAS_BENEFIT_COMPANY_ID);
        try {
            LOGGER.info(String.format("URL: %s", url));
            HttpResponse httpResponse = HttpUtil.sendRequest(HttpMethod.GET, "", url, MediaType.APPLICATION_JSON, DEFAULT_TIMEOUT, DEFAULT_TIMEOUT, DEFAULT_TIMEOUT,
                    HttpHeaders.AUTHORIZATION, accessToken);
            LOGGER.info(String.format("HTTP CODE: %d", httpResponse.getHttpCode()));
            LOGGER.info(String.format("RESPONSE: %s", httpResponse.getResponse()));
            if (httpResponse.getHttpCode() == 200) {
                LOGGER.info("Millas Benefit Report LQTXPS Generated Successfully");
                //sftp
                SFTPClient sft = new SFTPClient();
                sft.connect("", USERNAME_2, PASSWORD_2, HOST_2, 0);

                String fileName = httpResponse.getResponse();
                boolean isUpload = sft.upload(fileName, PATH_2);
                
                if(isUpload) {
                    LOGGER.info("Millas Benefit Report LQTXPS Uploaded Successfully");
                    String body = String.format("Los siguientes archivos:\n0172-MILLAS BENEFIT IBK\n%s\nFueron subidos al host con éxito.", fileName);
                    sendNotification(body, "Envio de Archivo Liquidacion POS Servicios - FINALIZADO OK", ARN_SNS_MILLASBENEFIT);
                } else {
                    String body = String.format("Hubo error en la subida del archivo al host: \n"
                        + "Archivo %s ", fileName);
                    sendNotification(body, "Envio de Archivo Liquidacion POS Servicios - ERROR", ARN_SNS_MILLASBENEFIT);
                    createErrorFile();
                }
            } else {
                LOGGER.error("Report generation failed.");
                sendNotification("Ocurrio un error al ejecutar reporte Millas Benefit", "Reporte MILLAS BENEFIT - ERROR", ARN_SNS_MILLASBENEFIT);
                createErrorFile();
            }
        } catch(JSchException | SftpException | IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            sendNotification("Ocurrio un error " + ex.getLocalizedMessage(), "Reporte MILLAS BENEFIT - ERROR", ARN_SNS_MILLASBENEFIT);
        }
    }
    
    public void createErrorFile() {
        try {
            //creating file error
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            SimpleDateFormat fileDate = new SimpleDateFormat("yyyyMMdd");
            File file = new File(String.format("/opt/output/0172_LQTXPS_%s.error", fileDate.format(calendar.getTime())));
            file.createNewFile();
            LOGGER.info(String.format("Error File: %s", file.getName()));
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage());
        }
    }
}
