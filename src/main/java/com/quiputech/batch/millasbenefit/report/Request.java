package com.quiputech.batch.millasbenefit.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "path", description = "path")
public class Request {
    private String path;
    private String reportType;

    @ApiModelProperty(value = "path", example = "/opt/output/0172_MILLASBENEFIT_20210805_TEST.DAT")
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    @ApiModelProperty(value = "reportType", example = "LQTXPS")
    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
}
