package com.quiputech.batch.millasbenefit.report;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.util.logging.Logger;

public class MySQLConnection {

    private static final Logger LOGGER = Logger.getLogger(MySQLConnection.class.getName());
    private static final String DATASOURCE = "java:/posServicios";

    public static Connection getConnection() {

        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup(DATASOURCE);

            return ds.getConnection();
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }
}
