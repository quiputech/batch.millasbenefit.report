package com.quiputech.batch.millasbenefit.report;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class MillasBenefitReportResource {

    @GET
    @Path("/execute")
    public Response execute() {
        MillasBenefitReport report = new MillasBenefitReport();
        report.executeReportLQTXPS();
        return Response.ok().build();
    }
    
    @GET
    @Path("/execute/consolidatedReport")
    public Response consolidatedReport() {
        MillasBenefitReport report = new MillasBenefitReport();
        report.executeConsolidatedReport();
        return Response.ok().build();
    }
    
    @GET
    @Path("/retry")
    public Response retry() {
        MillasBenefitReport report = new MillasBenefitReport();
        report.retry();
        return Response.ok().build();
    }
}
