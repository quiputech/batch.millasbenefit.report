package com.quiputech.batch.millasbenefit.report;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup
@Singleton
public class JobMillasBenefitReport {

    @Schedule(hour = "0", minute = "25", info = "Millas Benefit Report", persistent = false)
    public void executeConsolidatedReport() {
        MillasBenefitReport report = new MillasBenefitReport();
        report.executeConsolidatedReport();
    }
    
    @Schedule(hour = "0", minute = "37", info = "Millas Benefit Report", persistent = false)
    public void executeLQTXPS() {
        MillasBenefitReport report = new MillasBenefitReport();
        report.executeReportLQTXPS();
    }
    
    @Schedule(hour = "1", minute = "45", info = "Millas Benefit Report", persistent = false)
    public void executeRetry1() {
        MillasBenefitReport report = new MillasBenefitReport();
        report.retry();
    }
    
    @Schedule(hour = "2", minute = "00", info = "Millas Benefit Report", persistent = false)
    public void executeRetry2() {
        MillasBenefitReport report = new MillasBenefitReport();
        report.retry();
    }
}
