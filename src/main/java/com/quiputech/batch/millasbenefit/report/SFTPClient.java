package com.quiputech.batch.millasbenefit.report;

import com.jcraft.jsch.*;
import org.slf4j.LoggerFactory;

public class SFTPClient {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SFTPClient.class);
    private Session SESSION = null;

    public void connect(String privatekey, String username, String password, String host, int port) throws JSchException {
        try {
            JSch jsch = new JSch();

            // Uncomment the line below if the FTP server requires certificate
            if(!privatekey.isEmpty()) {
                jsch.addIdentity(privatekey);
            }

            // Comment the line above and uncomment the two lines below if the FTP server requires password
            if(port > 0){
                SESSION = jsch.getSession(username, host, port);
            } else {
                SESSION = jsch.getSession(username, host);
            }
            if(!password.isEmpty()) {
                SESSION.setPassword(password);
            }

            SESSION.setConfig("StrictHostKeyChecking", "no");
            SESSION.connect();
            LOGGER.info("SFTP CONNECTION SUCCESSFUL!!!");
        } catch(Exception e) {
            LOGGER.error("CONNECTION FAILED: " + e.getLocalizedMessage());
        }
    }

    public boolean upload(String source, String destination) throws JSchException, SftpException {
        Channel channel = null;
        ChannelSftp sftpChannel = null;
        boolean result = false;
        try {
            channel = SESSION.openChannel("sftp");
            channel.connect();
            sftpChannel = (ChannelSftp) channel;
            sftpChannel.put(source, destination);
            result = true;
            LOGGER.info("Conexion success");
        } catch(Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }
        finally {
            if(sftpChannel != null) {
                sftpChannel.exit();
            }
            if(channel != null && channel.isConnected()) {
                channel.disconnect();
            }
        }
        return result;
    }
}
