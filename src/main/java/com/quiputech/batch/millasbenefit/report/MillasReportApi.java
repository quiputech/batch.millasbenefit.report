package com.quiputech.batch.millasbenefit.report;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.quiputech.security.annotations.Logged;
import com.quiputech.security.annotations.NoSecurity;
import io.swagger.annotations.Api;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.core.Context;

/**
 *
 * @author Michael Galdámez
 */
@Path("")
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Millas Report", description = "RESTful API")
public class MillasReportApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(MillasReportApi.class);
    private static final ObjectMapper OM = new ObjectMapper();
    private static final boolean DEBUG = Boolean.parseBoolean(System.getProperty("debug", "false"));
    
    @POST()
    @Path("/uploadFile")
    @Produces("application/json")
    //@Logged
    @NoSecurity
    public Response execute(Request body, @Context HttpServletRequest servletRequest) {
        try {
            String fileName = body.getPath();
            String reportType = body.getReportType();
            MillasBenefitReport report = new MillasBenefitReport();
            report.upload(fileName, reportType);
            return Response.ok("").build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.serverError().entity(ex.getLocalizedMessage()).build();
        }
    }
    
    @POST()
    @Path("/uploadFile/s3")
    @Produces("application/json")
    //@Logged
    @NoSecurity
    public Response execute2(Request body, @Context HttpServletRequest servletRequest) {
        try {
            String fileName = body.getPath();
            MillasBenefitReport report = new MillasBenefitReport();
            report.uploadToS3(fileName, "Michael-Pruebas/testMichael.DAT");
            return Response.ok("").build();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            return Response.serverError().entity(ex.getLocalizedMessage()).build();
        }
    }
}
